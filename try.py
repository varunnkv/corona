from urllib2 import Request, urlopen
import json
import pandas as pd
import numpy as np
from pandas.io.json import json_normalize

def get():
	request=Request('https://api.covid19india.org/raw_data.json')
	response = urlopen(request)
	raw_data = response.read()
	data = json.loads(raw_data)
	return pd.DataFrame.from_dict(json_normalize(data['raw_data']), orient='columns')

def cleanup(df):
	df.replace(r'', np.NaN)
	df['agebracket'] = pd.to_numeric(df['agebracket'], errors='coerce')
	#more to do, of course. starting with changing dtypes
	return df

df = cleanup(get())
